#include "CommentCounter.h"
#include "FileBuffer.h"
#include <queue>
#include <sstream>

#include <fcntl.h>
#include <string.h>
#include <strings.h>

#include <boost/circular_buffer.hpp>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

std::ostream &operator<<(std::ostream &os, const SourceCodeStats &stats) {
  os << " total:" << stats.lines << " empty:" << stats.spacelines
     << " effective:" << stats.codes << " comments:" << stats.comments;
  return os;
}



void print(SmartCharPtr text, long index, long size) {
  std::stringstream ss;
  for (int i = index; i < index + size; i++) {
    ss << text[i];
  }
  std::cout << "[codesline" << index << "," << size << ")" << ss.str()
            << std::endl;
}

struct FileGrand{
  int _fd;
  FileGrand(int fd):_fd(fd){
  }
  ~FileGrand(){
    close(_fd);
  }
};

SourceCodeStats cppSourceCounter(const char *fileName) {
  SourceCodeStats stats;
  int fd = open(fileName, O_RDONLY);
  if (fd < 0) {
    throw std::runtime_error{"open failed "};
  }
  FileGrand g(fd);
  
    
  // get the size of fileName
  struct stat s;
  int status = fstat(fd, &s);
  if (status < 0) {
    throw std::runtime_error{" fstat failed "};
  }
  
  try{
    SmartCharPtr text(fd);
      
    CppCommentCounter<SmartCharPtr> counter;
    stats = counter.sourceCounter(text, s.st_size);

  }catch(std::exception& e){
    throw std::runtime_error{"mmap failed "};
  }


  return stats;
}

bool isCppFile(const char *path) {
  int len = strlen(path);
  if (len <= 2) {
    return false;
  }

  if (strcmp(path + len - 2, ".c") == 0 || strcmp(path + len - 2, ".H") == 0 ||
      strcmp(path + len - 2, ".h") == 0 || strcmp(path + len - 2, ".C") == 0) {
    return true;
  }
  
  if (len > 3 && strcmp(path + len - 3, ".cc") == 0) {
    return true;
  }

  if (len > 4 && (strcmp(path + len - 4, ".cpp") == 0 ||
                  strcmp(path + len - 4, ".cxx") == 0 ||
                  strcmp(path + len - 4, ".hpp") == 0)) {
    return true;
  }

  return false;
}
