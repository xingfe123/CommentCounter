#pragma once
#include "FileBuffer.h"
#include <exception>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include <stack>

#include <assert.h>
#include <string.h>
#include <strings.h>
void print(SmartCharPtr text, long offset, long size);

struct SourceCodeStats {
  long lines = 0;
  long comments = 0;
  long spacelines = 0;
  long codes = 0;
  friend std::ostream &operator<<(std::ostream &o, const SourceCodeStats &stats);
};

// if text[0-size] equal word, return length of word
// else return 0;
template<typename CharPtr>
int prefixEqual(CharPtr text, long size, const char *word) ;

template<typename CharPtr>
struct Comment {
  virtual int isNewLine(CharPtr text, long size) = 0;
  virtual int isCommentStart(CharPtr text, long size) = 0;
  virtual int isCommentEnd(CharPtr text, long size) = 0;
  virtual int isLineComment(CharPtr text, long size) = 0;

  bool isKey(CharPtr text, long size);
  int newLineCounter(CharPtr text, long size, SourceCodeStats *stats);
  int mutilineCommentsCounter(CharPtr text, long size, SourceCodeStats *stats);
  int effectiveCounter(CharPtr text, long size) ;
  
  SourceCodeStats sourceCounter(CharPtr text, long size);
};

template<typename CharPtr>
struct CppCommentCounter : public Comment<CharPtr> {

  // return length of newline string
  virtual int isNewLine(CharPtr text, long size) override;

  virtual int isCommentEnd(CharPtr text, long size) override;

  // return length of //
  virtual int isLineComment(CharPtr text, long size) override;

  // return length of /*
  virtual int isCommentStart(CharPtr text, long size) override;
  
};

SourceCodeStats cppSourceCounter(const char *fileName);

bool isCppFile(const char *path);

// 
// template  implement details 
#include "CommentCounter.hpp"
