#pragma once
#include <unistd.h>
#include <exception>
#include <memory>

struct LazyFileMap{
  char buffer[2048];
  int fileOffset;
  int _fileDes;
  ssize_t bufferSize;
LazyFileMap(int fileDes):_fileDes(fileDes){
    bufferSize = pread(_fileDes, buffer, sizeof(buffer), fileOffset);
    if (bufferSize < 0){
      throw std::runtime_error{""};
    }
  }
  char get(int index){
    if(index >= fileOffset &&
       index < fileOffset+bufferSize){
      return buffer[index-fileOffset];
    }

    fileOffset = index;
    bufferSize = pread(_fileDes, buffer, sizeof(buffer), fileOffset);
    if(bufferSize <0){
      throw std::runtime_error{""};
    }
    return buffer[0];
    
  }
};

struct SmartCharPtr{
  int offset = 0;
  std::shared_ptr<LazyFileMap> lazyFileMap;
SmartCharPtr(int fd):
  lazyFileMap(std::make_shared<LazyFileMap>(fd)){
    
  }
  SmartCharPtr operator+(int index){
    SmartCharPtr ptr(*this);
    ptr.offset+=index;
    return ptr;
  }
  char operator[](int index){
    return lazyFileMap->get(index+offset);
  }
  
};
