#pragma once
#include "CommentCounter.h"

template<typename CharPtr>
int prefixEqual(CharPtr text, long size, const char *word) {
  int len = strlen(word);
  if (size < len) {
    return 0;
  }

  for (int i = 0; word[i] != '\0'; i++) {
    if (text[i] != word[i]) {
      return 0;
    }
  }

  return len;
}



template<typename CharPtr>
SourceCodeStats Comment<CharPtr>::sourceCounter(CharPtr text, long size) {
  SourceCodeStats stats;
  if (size <= 0) {
    return stats;
  }

  long index = 0;
  while (index < size) {

    // ignore ' \t'
    if (text[index] == ' ' || text[index] == '\t') {
      index++;
      continue;
    }

    // meet //
    int commentLength = isLineComment(text + index, size - index);
    if (commentLength > 0) {
      // skip '//'
      stats.comments++;
      index += commentLength;


     // to newline
      while (index < size && !isNewLine(text + index, size - index)) {
        index++;
      }

      // eat one newLine;
      index += newLineCounter(text + index, size - index, &stats);
      continue;
    }

    // meet newline
    int newLineLength = isNewLine(text + index, size - index);
    if (newLineLength > 0) {
      stats.spacelines++;
      stats.lines++;
      index += newLineLength;
      continue;
    }

    // meet /*
    int commentStartLength = isCommentStart(text + index, size - index);
    if (commentStartLength > 0) {
      stats.comments++;
      index += commentStartLength;
      index += mutilineCommentsCounter(text + index, size - index, &stats);

      index += newLineCounter(text + index, size - index, &stats);
      continue;
    }

    // other words;
    stats.codes++;
    int offset = effectiveCounter(text + index, size - index);
    if (offset <= 0) {
      break;
    }
    
    // print(text, index, offset);

    index += offset;
    index += newLineCounter(text + index, size - index, &stats);
  }

  return stats;
}

template<typename CharPtr>
int Comment<CharPtr>::mutilineCommentsCounter(CharPtr text, long size,
                                              SourceCodeStats *stats) {
  bool prevCharIsNewLine = false;
  int offset = 0;

  while (offset < size) {
    int commentEndLength = isCommentEnd(text + offset, size - offset);
    if (commentEndLength > 0) {
      offset += commentEndLength;
      break;
    }

    int newLineLength = isNewLine(text + offset, size - offset);
    if (newLineLength == 0) {
      offset++;
      prevCharIsNewLine = false;
    } else {
      if (prevCharIsNewLine == true) {
        stats->spacelines++;
      } else {
        stats->comments++;
      }
      stats->lines++;
      offset += newLineLength;
      prevCharIsNewLine = true;
    }
  }
  return offset;
}

template<typename CharPtr>
int Comment<CharPtr>::effectiveCounter(CharPtr text, long size) {

  if (size <= 0) {
    return 0;
  }

  std::stack<char> matchs;
  int offset = 0;
  while (offset < size) {

    // find \?
    if (!matchs.empty() && matchs.top() == '\\') {
      matchs.pop();
      offset++;
      continue;
    }

    // find ''
    if (!matchs.empty() && matchs.top() == '\'') {
      if (text[offset] == '\'') {
        matchs.pop();
      } else if (text[offset] == '\\') {
        matchs.push(text[offset]);
      }
      offset++;
      continue;
    }

    //find ""
    if (!matchs.empty() && matchs.top() == '"') {
      if (text[offset] == '"') {
        matchs.pop();
      } else if (text[offset] == '\\') {
        matchs.push(text[offset]);
      }
      offset++;
      continue;
    }

    if (text[offset] == '\\' || text[offset] == '\'' || text[offset] == '"') {

      matchs.push(text[offset]);
      offset++;
      continue;
    }

    if (isKey(text + offset, size - offset)) {
      return offset;
    }

    // 忽略其他字符。
    offset++;
  }
  
  return 0;
}


template<typename CharPtr>
int Comment<CharPtr>::newLineCounter(CharPtr text, long size,
                                     SourceCodeStats *stats) {
  int newLineLength = isNewLine(text, size);
  if (newLineLength > 0) {
    stats->lines++;
  }
  return newLineLength;
}

template<typename CharPtr>
int CppCommentCounter<CharPtr>::isLineComment(CharPtr text, long size) {
  if (size < 2) {
    return 0;
  }
  return prefixEqual(text, size, "//");
}

template<typename CharPtr>
int CppCommentCounter<CharPtr>::isNewLine(CharPtr text, long size) {
  if (prefixEqual(text, size, "\r\n") || prefixEqual(text, size, "\n\r")) {
    return 2;
  } else if (prefixEqual(text, size, "\r") || prefixEqual(text, size, "\n")) {
    return 1;
  }
  return 0;
}

// return length of */
template<typename CharPtr>
int CppCommentCounter<CharPtr>::isCommentEnd(CharPtr text, long size) {
  if (size < 2) {
    return 0;
  }
  return prefixEqual(text, size, "*/");
}

template<typename CharPtr>
int CppCommentCounter<CharPtr>::isCommentStart(CharPtr text, long size) {
  if (size < 2) {
    return 0;
  }
  return prefixEqual(text, size, "/*");
}

template<typename CharPtr>
bool Comment<CharPtr>::isKey(CharPtr text, long size) {
  if (isNewLine(text, size)) {
    return true;
  }

  if (isCommentStart(text, size)) {
    return true;
  }

  if (isLineComment(text, size)) {
    return true;
  }
  
  return false;
}
