#include <gtest/gtest.h>
#include <iostream>
#include <list>

std::vector<uint64_t> primes_cache;

bool isNextPrime(uint64_t top) {
  if(primes_cache.empty()){
    return true;
  }
  
  if (top <= primes_cache[primes_cache.size() - 1]) {
    return false;
  }

  for (auto p : primes_cache) {
    if (top % p == 0) {
      return false;
    }
  }
  return true;
}

void get_all_prime_divisors(uint64_t n) {
  std::list<uint64_t> divisors;
  std::vector<bool> primesMap(n+1);
  
  primesMap[0] = false;
  primesMap[1] = false;
  for(int i=2;i<=n;i++){
    primesMap[i] = true;
  }
  for(int i=2;i<=n;i++){
    if(primesMap[i]==true){
      for(int j=2;j*i<=n;j++)
        primesMap[i*j]=false;
    }
  }
  
  for(int i=2;i<=n;i++){
    if(primesMap[i]==false){
      continue;
    }
    
    while(n % i == 0){
      n = n / i;
      if (divisors.back() != i){
        divisors.push_back(i);
      }
    }
  }

  // When you finish, go for WHITE-RABBIT(12)

  //divisors.sort(MoreThan());
  for (auto a :  divisors){
    std::cout << a << std::endl;
  }

  //return divisors;
}

TEST(IsAbsTest, PrimeTest) {
  get_all_prime_divisors(36);
  get_all_prime_divisors(123131231UL);
}
