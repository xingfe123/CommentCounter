#include "CommentCounter.h"
#include "FileBuffer.h"
#include <gtest/gtest.h>
#include <iostream>

//CppCommentCounter<SmartCharPtr> counter
CppCommentCounter<char*> counter;

template<typename CharPtr>
SourceCodeStats sourceCounter(CharPtr text, long size) {
  CppCommentCounter<CharPtr> counter;
  return counter.sourceCounter(text, size);
}

TEST(IsAbsTest, prefixEqual) {
  char buffer[128];
  snprintf(buffer, sizeof(buffer), "%s", "//\n");
  int size = strlen(buffer);

  int index = 0;
  bool b = prefixEqual(buffer, size - index, "//");
  ASSERT_EQ(b, true);
}

TEST(IsAbsTest, isCppFile) {
  ASSERT_EQ(isCppFile("a.c"), true);
  ASSERT_EQ(isCppFile("a.cc~"), false);
  ASSERT_EQ(isCppFile("a.hpp"), true);
  ASSERT_EQ(isCppFile("a.h~"), false);
}
TEST(IsAbsTest, lineCounter) {
  char buffer[128];
  snprintf(buffer, sizeof(buffer), "%s", "\n\n");

  SourceCodeStats stat = sourceCounter(buffer, strlen(buffer));
  ASSERT_EQ(stat.lines, 3);
  ASSERT_EQ(stat.spacelines, 2);
}

TEST(IsAbsTest, lineCounter1) {
  char buffer[128];
  snprintf(buffer, sizeof(buffer), "%s", "\n");
  int size = strlen(buffer);

  int index = 0;
  int newLineLength = counter.isNewLine(buffer + index, size - index);
  ASSERT_EQ(newLineLength, 1);

  SourceCodeStats stat = sourceCounter(buffer, size);
  ASSERT_EQ(stat.lines, 2);
}

TEST(IsAbsTest, lineCounter2) {
  char buffer[128];
  snprintf(buffer, sizeof(buffer), "%s", "\n/**/\n\n");

  SourceCodeStats stat = sourceCounter(buffer, strlen(buffer));
  ASSERT_EQ(stat.lines, 4);
  ASSERT_EQ(stat.spacelines, 2);
  ASSERT_EQ(stat.comments, 1);
}

TEST(IsAbsTest, lineCounter3) {
  char buffer[128];
  snprintf(buffer, sizeof(buffer), "%s", "/**/");
  int commentStartLength = counter.isCommentStart(buffer, strlen(buffer));
  ASSERT_EQ(commentStartLength, 2);

  SourceCodeStats stat = sourceCounter(buffer, strlen(buffer));
  ASSERT_EQ(stat.lines, 1);
  ASSERT_EQ(stat.spacelines, 0);
  ASSERT_EQ(stat.comments, 1);
}

TEST(IsAbsTest, lineCounter4) {
  char buffer[128];
  snprintf(buffer, sizeof(buffer), "%s", "/*\n*/");
  int commentStartLength = counter.isCommentStart(buffer, strlen(buffer));
  ASSERT_EQ(commentStartLength, 2);

  SourceCodeStats stat = sourceCounter(buffer, strlen(buffer));
  ASSERT_EQ(stat.lines, 2);
  ASSERT_EQ(stat.spacelines, 0);
  ASSERT_EQ(stat.comments, 2);
}

TEST(IsAbsTest, lineCounter5) {
  char buffer[128];
  snprintf(buffer, sizeof(buffer), "%s", "/*\n*\n*/");
  int commentStartLength = counter.isCommentStart(buffer, strlen(buffer));
  ASSERT_EQ(commentStartLength, 2);

  SourceCodeStats stat = sourceCounter(buffer, strlen(buffer));
  ASSERT_EQ(stat.lines, 3);
  ASSERT_EQ(stat.spacelines, 0);
  ASSERT_EQ(stat.comments, 3);
}

TEST(IsAbsTest, lineCounter6) {
  char buffer[128];
  snprintf(buffer, sizeof(buffer), "%s", "/*\n\n*\n*/");
  int commentStartLength = counter.isCommentStart(buffer, strlen(buffer));
  ASSERT_EQ(commentStartLength, 2);

  SourceCodeStats stat = sourceCounter(buffer, strlen(buffer));
  ASSERT_EQ(stat.lines, 4);
  ASSERT_EQ(stat.spacelines, 1);
  ASSERT_EQ(stat.comments, 3);
}

// TEST(IsAbsTest, lineCounter7) {
//   char buffer[128];
//   snprintf(buffer, sizeof(buffer), "%s", "///*\r\n/*\n*\n*/");

//   SourceCodeStats stat = sourceCounter(buffer, strlen(buffer));
//   ASSERT_EQ(stat.lines, 3);
//   ASSERT_EQ(stat.spacelines, 0);
//   ASSERT_EQ(stat.comments, 4);
// }

TEST(IsAbsTest, test_cc) {

  SourceCodeStats stat = cppSourceCounter("../test.cc");
  // ASSERT_EQ(stat.lines, 5);

  ASSERT_EQ(stat.codes, 2);
  ASSERT_EQ(stat.spacelines, 1);
  ASSERT_EQ(stat.comments, 7);
}

TEST(IsAbsTest, CommentCounter_cc) {

  SourceCodeStats stat = cppSourceCounter("../CommentCounter.cc");
  ASSERT_EQ(stat.comments, 1);
  ASSERT_EQ(stat.spacelines, 15);
  // ASSERT_EQ(stat.lines, 62);
  // ASSERT_EQ(stat.lines, stat.comments + stat.spacelines + stat.codes);
  ASSERT_EQ(stat.codes, 56);
}

TEST(IsAbsTest, CommentCounter_H) {

  SourceCodeStats stat = cppSourceCounter("../CommentCounter.h");
  ASSERT_EQ(stat.comments, 19);
  ASSERT_EQ(stat.spacelines, 51);

  ASSERT_EQ(stat.codes, 205);
}

TEST(IsAbsTest, lineCounterTest_cc) {

  SourceCodeStats stat = cppSourceCounter("../CommentCounterTest.cc");
  ASSERT_EQ(stat.codes, 118);
  ASSERT_EQ(stat.spacelines, 40);
  ASSERT_EQ(stat.comments, 11);
}

TEST(IsAbsTest, HandlerTrueReturn) {
  char buffer[128];
  snprintf(buffer, sizeof(buffer), "%s", "//\n");
  int size = strlen(buffer);

  int index = 0;
  int commentStartLength = counter.isLineComment(buffer, size - index);
  ASSERT_EQ(commentStartLength, 2);
}


