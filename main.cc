#include "CommentCounter.h"
#include "FileBuffer.h"
#include <string.h>
#include <string>

#include <dirent.h>
#include <fstream>
#include <iostream>
#include <queue>
#include <sstream>
#include <sys/types.h>
#include <thread>
#include <vector>

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int argvDirNameLength = 0;

void handleDir(const char *dirName) {
  DIR *dir = opendir(dirName);
  if (dir == NULL) {
    throw std::runtime_error{"require arg[1] as fileName"};
  }

  while (true) {
    struct dirent *ptr = readdir(dir);
    if (ptr == NULL) {
      break;
    }

    //跳过'.'和'..'两个目录
    if (ptr->d_name[0] == '.') {
      continue;
    }

    // merge path
    char path[PATH_MAX + 1];
    bzero(path, sizeof(path));
    if (dirName[strlen(dirName) - 1] == '/') {
      snprintf(path, sizeof(path), "%s%s", dirName, ptr->d_name);
    } else {
      snprintf(path, sizeof(path), "%s/%s", dirName, ptr->d_name);
    }

    struct stat sb;
    lstat(path, &sb);
    if (S_ISREG(sb.st_mode) && isCppFile(path)) {

      std::string ourPath(path);
      std::thread t([ourPath] {
        try {
          std::stringstream ss;
          ss << "file:"
             << ourPath.substr(argvDirNameLength,
                               ourPath.size() - argvDirNameLength)
             << cppSourceCounter(ourPath.c_str()) << std::endl;

          // << is atomic operation
          std::cout << ss.str();
        } catch (std::exception &e) {
          std::cout << "handle path failed " << e.what() << std::endl;
        }
      });

      t.detach();

    } else if (S_ISDIR(sb.st_mode)) {
      handleDir(path);
    }
  }
  closedir(dir);
}

int main(int argc, char **argv) {
  if (argc < 2 || strlen(argv[1]) <= 0) {
    std::cout << "require arg[1] as fileName" << std::endl;
    return -1;
  }
  argvDirNameLength = strlen(argv[1]);
  handleDir(argv[1]);

  return 0;
}
